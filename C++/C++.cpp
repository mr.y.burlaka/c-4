﻿// C++.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <time.h>

int main()
{
	const int size = 5; 
	int sum = 0;

	struct tm buf;            
	time_t t = time(NULL);
	localtime_s(&buf, &t);
	int DayNum = buf.tm_mday;
	int Index = DayNum % size;

	int array[size][size];

	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			array[i][j] = i + j;
			std::cout << array[i][j] << ' ';
		}
		std::cout << "\n";
	}
	
	for (int j = 0; j < size; j++)
	{
		sum += array[Index][j];
	}
	std::cout << "Sum of element in row " << Index << ": " << sum << "\n";
}


